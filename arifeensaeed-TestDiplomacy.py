from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve
from unittest import main, TestCase
from io import StringIO
9  # !/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Istanbul Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ['A', 'Istanbul', 'Hold'])

    def test_read_2(self):
        s = "B Makkah Move Medina\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ['B', 'Makkah', 'Move', 'Medina'])


    def test_read_3(self):
        s = "C Lahore Support Austin\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ['C', 'Lahore', 'Support', 'Austin'])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'L', 'LosAngeles')
        self.assertEqual(w.getvalue(), "L LosAngeles\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 'D', 'Dallas')
        self.assertEqual(w.getvalue(), "D Dallas\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 'Z', '[dead]')
        self.assertEqual(w.getvalue(), "Z [dead]\n")

    # -----
    # solve
    # -----

    # invalid action 
    def test_solve_0(self):
        r = StringIO("A SanDiego Hold\nB Shanghai Dance\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A SanDiego\n")
    
    # one input only
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    # all armies holding
    def test_solve_2(self):
        r = StringIO("B Beirut Hold\nA Cairo Hold\nC Seattle Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Cairo\nB Beirut\nC Seattle\n")

    # only movements to (mutually exclusive) cities
    def test_solve_3(self):
        r = StringIO("A Austin Move Berlin\nB Lahore Move Beijing\nC Seattle Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Berlin\nB Beijing\nC Seattle\n")

    # move, hold, and then move (demonstrating input order does not matter)
    def test_solve_4(self):
        r = StringIO("A Madrid Move London\nB London Hold\nC Paris Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Madrid\n")

    # no input (quit gracefully)
    def test_solve_5(self):
        r = StringIO("\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "") 

    # Austin is supporting, but then attacked
    def test_solve_6(self):
        r = StringIO("B London Hold\nA Austin Support B\nC Dallas Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\nC [dead]\n")

    # one input - move
    def test_solve_7(self):
        r = StringIO("B Beijing Move Atlanta\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "B Atlanta\n")

    # one input - support
    def test_solve_8(self):
        r = StringIO("F Frankfurt Support Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "F Frankfurt\n")

    # movement loop (with unconsequential support)
    def test_solve_9(self):
        r = StringIO("A London Move Barcelona\nB Barcelona Move London\nC Amsterdam Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB London\nC Amsterdam\n")

    # support loop 
    def test_solve_10(self):
        r = StringIO("A Detroit Support B\nB DC Support A\nC Kiev Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Detroit\nB DC\nC Kiev\n")

    # support loop with outside attack
    def test_solve_11(self):
        r = StringIO("A Detroit Support B\nB DC Support A\nC Kiev Move Detroit\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Detroit\nB DC\nC [dead]\n")

    # all 4 armies dead (due to support while under attack)
    def test_solve_12(self):
        r = StringIO("A Houston Move Madrid\nB Madrid Support D\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # Normal test case, clear winner in a contested location and a supporting location not under attack - perfect
    def test_solve_13(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    # a complex test case with one location having support_scores from four different armies
    # being 1, 2, 3, 3... but since no one clear winner, all still dead in NewYork
    def test_solve_14(self):
        r = StringIO("A NewYork Hold\nB Houston Support A\nC LosAngeles Move NewYork\nD Dallas Support C\nE Beijing Move NewYork\nF Lahore Move NewYork\nG Austin Support E\nH Seattle Support F\nI Karachi Support E\nJ Beirut Support F\nK Medina Support C\nL Mumbai Support F\nM Nairobi Support E\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Houston\nC [dead]\nD Dallas\nE [dead]\nF [dead]\nG Austin\nH Seattle\nI Karachi\nJ Beirut\nK Medina\nL Mumbai\nM Nairobi\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""